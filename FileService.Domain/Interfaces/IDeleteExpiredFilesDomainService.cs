namespace FileService.Domain.Interfaces
{
    public interface IDeleteExpiredFilesDomainService
    {
        void DeleteAnExpiredFile();
    }
}